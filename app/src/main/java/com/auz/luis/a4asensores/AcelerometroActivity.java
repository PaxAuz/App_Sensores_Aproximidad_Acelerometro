package com.auz.luis.a4asensores;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class AcelerometroActivity extends AppCompatActivity implements SensorEventListener{

    TextView textViewEjeX, textViewEjeY, textViewEjeZ;
    SensorManager sensorManager;
    Sensor sensorAcelerometro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acelerometro);

        textViewEjeX = (TextView) findViewById(R.id.TextViewEjeX);
        textViewEjeY = (TextView) findViewById(R.id.TextViewEjeY);
        textViewEjeZ = (TextView) findViewById(R.id.TextViewEjeZ);

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorAcelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }
    @Override
    protected void onResume(){
        super.onResume();
        sensorManager.registerListener(this, sensorAcelerometro, SensorManager.SENSOR_DELAY_FASTEST);
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        textViewEjeX.setText(Float.toString( event.values[0]));
        textViewEjeY.setText(Float.toString( event.values[1]));
        textViewEjeZ.setText(Float.toString( event.values[2]));


    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
